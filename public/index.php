<?php

header('Content-Type: text/html; charset=UTF-8');

$ability_labels = ['zawarudo' => 'Остановка времени', 'zahando' => 'Стирание пространства', 'holopsicon' => '66 движений космоса', 'Aa' => 'Всемогущество'];

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  
  if (!empty($_GET['save'])) {
   
    print('Спасибо, результаты сохранены.');
  }
 
  include('form.php');
  
  exit();
}

$errors = FALSE;

if (empty($_POST['fio'])) {
  print('Введите имя.<br/>');
  $errors = TRUE;
}
else if (!preg_match('/^[а-яА-Я ]+$/u', $_POST['fio'])) {
  print('Недопустимые символы в имени.<br/>');
  $errors = TRUE;
}

if (empty($_POST['year'])) {
    print('Введите год.<br/>');
    $errors = TRUE;
}
else {
  $year = $_POST['year'];
  if (!(is_numeric($year) && intval($year) >= 1800 && intval($year) <= 2020)) {
    print('Укажите корректный год.<br/>');
    $errors = TRUE;
  }
}

$ability_data = array_keys($ability_labels);
if (empty($_POST['abilities'])) {
    print('Выберите способность.<br/>');
    $errors = TRUE;
}
else{
  $abilities = $_POST['abilities'];
  foreach ($abilities as $ability) {
    if (!in_array($ability, $ability_data)) {
      print('Плохая способность!<br/>');
      $errors = TRUE;
    }
  }
}
$ability_insert = [];
foreach ($ability_data as $ability) {
  $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
}



if ($errors) {
  exit();
}



$user = 'u17450';
$pass = '2696137';
$db = new PDO('mysql:host=localhost;dbname=u17450', $user, $pass,
  array(PDO::ATTR_PERSISTENT => true));


try {
  $stmt = $db->prepare("INSERT INTO application SET name = ?, year = ?, ability_zawarudo = ?, ability_zahando = ?, ability_holopsicon = ?, ability_aa = ?");
  $stmt->execute([$_POST['fio'], intval($year), $ability_insert['zawarudo'], $ability_insert['zahando'], $ability_insert['holopsicon'], $ability_insert['aa']]);
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}


header('Location: ?save=2');
